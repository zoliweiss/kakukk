#include <nvs_flash.h>
#include <nvs.h>
#include <xtensa/core-macros.h>
#include <time.h>
#include <HTTPClient.h>
#include <WiFi.h>
#include <esp_wifi.h>
#include <Wire.h>
#include <Stepper.h>
#include <Rtc_Pcf8563.h>

#include "rs/rs.hpp"
#include "sound.h"
#include "private_conf.h"

#define DAC_PIN          25
#define MUTE_PIN         27
#define BATTERY_MEAS_PIN  4
#define STEPPER_A1_PIN   19
#define STEPPER_A2_PIN   18
#define STEPPER_B1_PIN   17
#define STEPPER_B2_PIN   16
#define MARCH             3
#define OCTOBER          10
#define SATURDAY          6
#define DST_AUTO         -1
#define CENTURY_20TH      1
#define CENTURY_21TH      0

#define CUCKOO_PRESENCE_MS       1000
#define WIFI_TIMEOUT_SECONDS       10
#define FREE_RTOS_MAGIC_DELAY      86
#define STEPPER_RPM                70
#define STEPPER_DESIRED_ANGLE     120
#define STEPPER_STEPS_PER_REV      48
#define NTP_SYNC_DAY                0
#define NTP_SYNC_HOUR              12
#define BATTERY_MEAS_HOUR          11
#define WAKE_UP_MIN                 0
#define BATTERY_HALT_VOLT_MV     4600
#define BATTERY_OK_VOLT_MV       5200
#define BATTERY_MEAS_FINE_TUNE      1.025f
#define NTP_SERVER                "pool.ntp.org"
#define TIMEZONE_ENV              "CET-1CEST-2,M3.5.0/02:00:00,M10.5.0/03:00:00"
#define NUMBER_OF_NVS_RECORDS_KEY "num_of_records"

const uint8_t g_activity_table[] = {                                      /*
  0  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 20 21 22 23  */
  0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 3, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, // monday
  0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 3, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, // tuesday
  0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 3, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, // wednesday
  0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 3, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, // thursday
  0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 3, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, // friday
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 3, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, // saturday
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 3, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0  // sunday
};

Rtc_Pcf8563 g_rtc;
RS::ReedSolomon<6, 2> g_rs;
Stepper g_stepper(STEPPER_STEPS_PER_REV, STEPPER_A1_PIN, STEPPER_B1_PIN, STEPPER_A2_PIN, STEPPER_B2_PIN);

void setup()
{
  setbuf(stdout, NULL);

  g_stepper.setSpeed(STEPPER_RPM);
  g_rtc.getDateTime();

  print_debug();
  store_battery_voltage();
  const uint8_t l_num_of_repetitions = get_number_of_repetitions();

  if(WL_CONNECTED == WiFi.status())
  {
    log_battery_voltages();
    stop_radio();
  }

  for(uint8_t i = 0; i < l_num_of_repetitions; i++)
  {
    show_kakukk();
    delay(250);
    play_sound();
    delay(250);
    hide_kakukk();

    if(i < l_num_of_repetitions - 1)
    {
      delay(CUCKOO_PRESENCE_MS);
    }
  }

  nvs_close(get_nvs_handle());
  const uint8_t l_next_alarm_hour = get_next_alarm_hour(); /* save alarm time before setting DST compensation */
  set_dst_compensated_time_if_needed();

  if(get_battery_voltage() < BATTERY_HALT_VOLT_MV &&
     !is_battery_low_indication_set())
  {
    show_kakukk();
    set_battery_low_indication();
  }

  g_rtc.setAlarm(WAKE_UP_MIN, l_next_alarm_hour, RTCC_NO_ALARM, RTCC_NO_ALARM);
}

void loop()
{
}

nvs_handle get_nvs_handle()
{
  static nvs_handle l_handle = 0;

  if(ESP_OK != nvs_open("voltages", NVS_READWRITE, &l_handle))
  {
    esp_err_t l_status = nvs_flash_init();

    if(l_status == ESP_ERR_NVS_NO_FREE_PAGES)
    {
      ESP_ERROR_CHECK(nvs_flash_erase());
      l_status = nvs_flash_init();
    }

    ESP_ERROR_CHECK(l_status);

    printf("Opening NVS handle... ");
    l_status = nvs_open("voltages", NVS_READWRITE, &l_handle);

    if(l_status == ESP_OK)
    {
      printf("Done\n");
    }
    else
    {
      printf("Error (%s) opening NVS handle!\n", esp_err_to_name(l_status));
    }
  }

  return l_handle;
}

time_t now()
{
  struct tm l_timeinfo;
  memset(&l_timeinfo, 0, sizeof(l_timeinfo));

  if(!is_rtc_data_valid())
  {
    return 0;
  }

  l_timeinfo.tm_isdst = DST_AUTO;
  l_timeinfo.tm_mday = g_rtc.getDay();
  l_timeinfo.tm_wday = 0;
  l_timeinfo.tm_mon = g_rtc.getMonth() - 1;
  l_timeinfo.tm_year = g_rtc.getYear() + 100;
  l_timeinfo.tm_hour = g_rtc.getHour();
  l_timeinfo.tm_min = g_rtc.getMinute();
  l_timeinfo.tm_sec = g_rtc.getSecond();

  setenv("TZ", TIMEZONE_ENV, 1);
  tzset();

  return mktime(&l_timeinfo);
}

uint8_t get_compensated_hour()
{
  if(30 > g_rtc.getMinute())
  {
    return g_rtc.getHour();
  }
  else
  {
    return (g_rtc.getHour() + 1) % 24;
  }
}

uint16_t get_battery_voltage()
{
  static uint16_t l_battery_voltage = 0;

  if(0 == l_battery_voltage)
  {
    pinMode(BATTERY_MEAS_PIN, INPUT);
    l_battery_voltage = (uint16_t)(2 * BATTERY_MEAS_FINE_TUNE * 3300.0f * analogRead(BATTERY_MEAS_PIN) / 4095.5f);
  }

  return l_battery_voltage;
}

uint32_t get_number_of_nvs_records()
{
  uint32_t l_number_of_nvs_records = 0;

  nvs_get_u32(get_nvs_handle(), NUMBER_OF_NVS_RECORDS_KEY, &l_number_of_nvs_records);

  return l_number_of_nvs_records;
}

void set_number_of_nvs_records(const uint32_t number_of_nvs_records)
{
  nvs_set_u32(get_nvs_handle(), NUMBER_OF_NVS_RECORDS_KEY, number_of_nvs_records);
}

void log_battery_voltages()
{
  uint64_t l_data = 0;
  uint64_t l_data_encoded = 0;
  esp_err_t l_status = ESP_OK;
  String l_post_data;
  HTTPClient l_http;
  int16_t l_result = -1;

  for(uint32_t l_key = 0; ESP_OK == (l_status = nvs_get_u64(get_nvs_handle(), String(l_key).c_str(), &l_data_encoded)); l_key++)
  {
    if(RESULT_SUCCESS == g_rs.Decode(&l_data_encoded, &l_data))
    {
      l_post_data += (uint32_t)(l_data & 0xFFFF);
      l_post_data += ":";
      l_post_data += (uint32_t)(l_data >> 16 & 0xFFFFFFFF);
      l_post_data += "|";
    }
  }

  if(ESP_ERR_NVS_NOT_FOUND != l_status && ESP_OK != l_status)
  {
    printf("Error (%s) reading!\n", esp_err_to_name(l_status));
    nvs_erase_all(get_nvs_handle());
    nvs_commit(get_nvs_handle());
  }

  if(l_post_data.length())
  {
    l_http.begin(g_log_server);
    l_http.addHeader("Content-Type", "application/x-www-form-urlencoded");
    l_result = l_http.POST("data=" + l_post_data);
    l_http.end();

    printf("HTTP result: %d\n", l_result);

    if(HTTP_CODE_OK == l_result)
    {
      nvs_erase_all(get_nvs_handle());
      nvs_commit(get_nvs_handle());
    }
  }
}

void store_battery_voltage()
{
  const time_t l_timestamp = now();
  const uint16_t l_battery_voltage = get_battery_voltage();

  if(!l_timestamp || BATTERY_MEAS_HOUR != get_compensated_hour())
  {
    return;
  }

  esp_err_t l_status = ESP_OK;
  uint64_t l_data = (uint64_t)l_timestamp << 16 | l_battery_voltage;
  uint64_t l_data_encoded = 0;
  uint32_t l_number_of_nvs_records = get_number_of_nvs_records();

  g_rs.Encode(&l_data, &l_data_encoded);

  l_status = nvs_set_u64(get_nvs_handle(), String(l_number_of_nvs_records).c_str(), l_data_encoded);
  printf("Updating data in NVS ... ");
  printf((l_status == ESP_OK) ? "Done\n" : "FAILED!\n");

  printf("Committing updates in NVS ... ");
  l_status = nvs_commit(get_nvs_handle());
  printf((l_status == ESP_OK) ? "Done\n" : "FAILED!\n");

  set_number_of_nvs_records(l_number_of_nvs_records + 1);
}

bool is_rtc_data_valid()
{
  return !g_rtc.getVoltLow() && g_rtc.getYear() > 19 && g_rtc.getYear() < 30;
}

bool is_ntp_sync_needed()
{
  return !is_rtc_data_valid() || (NTP_SYNC_HOUR == get_compensated_hour() && NTP_SYNC_DAY == g_rtc.getWeekday());
}

bool do_ntp_sync()
{
  struct tm l_timeinfo;

  if(connect_to_wifi())
  {
    configTime(0, 0, NTP_SERVER);
    setenv("TZ", TIMEZONE_ENV, 1);
    tzset();
  }

  if(!getLocalTime(&l_timeinfo))
  {
    printf("NTP FAILURE\n");
    return false;
  }

  print_time(l_timeinfo);

  g_rtc.setDateTime(l_timeinfo.tm_mday,
                    l_timeinfo.tm_wday,
                    l_timeinfo.tm_mon + 1,
                    CENTURY_21TH,
                    l_timeinfo.tm_year - 100,
                    l_timeinfo.tm_hour,
                    l_timeinfo.tm_min,
                    l_timeinfo.tm_sec);

  return true;
}

uint8_t get_number_of_repetitions()
{
  uint8_t l_ntp_sync_try_number = 0;
  uint8_t l_ntp_sync_try_limit = 10;
  bool l_ntp_sync_success = false;

  if(is_battery_low_indication_set() &&
     BATTERY_OK_VOLT_MV > get_battery_voltage())
  {
    return 0;
  }

  if(is_ntp_sync_needed())
  {
    if(is_rtc_data_valid())
    {
      l_ntp_sync_try_limit = 1;
    }

    while(l_ntp_sync_try_number < l_ntp_sync_try_limit &&
          !l_ntp_sync_success)
    {
      printf("NTP sync attempt: %d\n", ++l_ntp_sync_try_number);
      l_ntp_sync_success = do_ntp_sync();
    }
  }

  if(!is_rtc_data_valid())
  {
    printf("NTP data invalid after NTP sync!\n");
    return 0;
  }

  return g_activity_table[((g_rtc.getWeekday() + 6) % 7) * 24 + get_compensated_hour()];
}

uint8_t get_next_alarm_hour()
{
  uint8_t l_hour_incr = 1;
  const uint8_t l_dow = (g_rtc.getWeekday() + 6) % 7;
  const uint8_t l_hour = get_compensated_hour();

  while(0 == g_activity_table[(l_dow * 24 + l_hour + l_hour_incr) % 168])
  {
    l_hour_incr++;
  }

  return (l_hour + l_hour_incr) % 24;
}

void print_debug()
{
  struct tm l_timeinfo;
  memset(&l_timeinfo, 0, sizeof(l_timeinfo));

  printf("Actual time from RTC: ");

  l_timeinfo.tm_isdst = DST_AUTO;
  l_timeinfo.tm_mday = g_rtc.getDay();
  l_timeinfo.tm_wday = g_rtc.getWeekday();
  l_timeinfo.tm_mon = g_rtc.getMonth() - 1;
  l_timeinfo.tm_year = g_rtc.getYear() + 100;
  l_timeinfo.tm_hour = g_rtc.getHour();
  l_timeinfo.tm_min = g_rtc.getMinute();
  l_timeinfo.tm_sec = g_rtc.getSecond();

  print_time(l_timeinfo);

  if(is_rtc_data_valid())
  {
    printf("Date is valid\n");
  }
  else
  {
    printf("Date is invalid\n");
  }

  printf("Battery voltage: %u\n", get_battery_voltage());

  if(is_battery_low_indication_set())
  {
    printf("Low voltage indication is set\n");
  }
  else
  {
    printf("Low voltage indication is not set\n");
  }

  printf("NVS records stored: %u\n", get_number_of_nvs_records());
}

void print_time(const struct tm timeinfo)
{
  const char *l_weekday[] = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};

  printf("%04d/%02d/%02d %02d:%02d:%02d, %s\n",
         timeinfo.tm_year + 1900,
         timeinfo.tm_mon + 1,
         timeinfo.tm_mday,
         timeinfo.tm_hour,
         timeinfo.tm_min,
         timeinfo.tm_sec,
         l_weekday[timeinfo.tm_wday]);
}

bool connect_to_wifi()
{
  if(WL_CONNECTED == WiFi.status())
  {
    printf("Already connected to %s\n", g_ssid);

    return true;
  }

  uint32_t l_start = millis();

  printf("Connecting to %s ", g_ssid);
  WiFi.begin(g_ssid, g_password);

  while(WL_CONNECTED != WiFi.status())
  {
    delay(500);
    printf(".");

    if(l_start + WIFI_TIMEOUT_SECONDS * 1000 < millis())
    {
      printf(" FAILED!\n");
      return false;
    }
  }

  printf(" Done\n");

  return true;
}

void play_sound()
{
  uint32_t l_wait_start = 0;
  uint32_t l_num_tick_to_wait = F_CPU / g_sample_rate;

  unmute_speaker();
  pinMode(DAC_PIN, OUTPUT);

  for(int i = 0; i < g_sample_count; i++)
  {
    l_wait_start = xthal_get_ccount();
    dacWrite(DAC_PIN, g_sound_samples[i] + 128);

    while((xthal_get_ccount() - l_wait_start) < (l_num_tick_to_wait - FREE_RTOS_MAGIC_DELAY))
    {
      NOP();
    }
  }

  mute_speaker();
}

void show_kakukk()
{
  mount_stepper();
  g_stepper.step(STEPPER_DESIRED_ANGLE / 360.0 * STEPPER_STEPS_PER_REV);
  unmount_stepper();
}

void hide_kakukk()
{
  mount_stepper();
  g_stepper.step(-STEPPER_DESIRED_ANGLE / 360.0 * STEPPER_STEPS_PER_REV);
  unmount_stepper();
}

void mount_stepper()
{
  pinMode(STEPPER_A1_PIN, OUTPUT);
  pinMode(STEPPER_A2_PIN, OUTPUT);
  pinMode(STEPPER_B1_PIN, OUTPUT);
  pinMode(STEPPER_B2_PIN, OUTPUT);
}

void unmount_stepper()
{
  pinMode(STEPPER_A1_PIN, INPUT);
  pinMode(STEPPER_A2_PIN, INPUT);
  pinMode(STEPPER_B1_PIN, INPUT);
  pinMode(STEPPER_B2_PIN, INPUT);
}

void mute_speaker()
{
  pinMode(MUTE_PIN, INPUT);
}

void unmute_speaker()
{
  digitalWrite(MUTE_PIN, HIGH);
  pinMode(MUTE_PIN, OUTPUT);
}

void stop_radio()
{
  esp_wifi_disconnect();
  esp_wifi_stop();
}

bool is_saturday_before_last_sunday_in_march_or_october(const uint8_t day, const uint8_t wday)
{
  return day > 23 && day < 31 && SATURDAY == wday;
}

int8_t get_dst_compensation(const uint8_t month, const uint8_t day, const uint8_t wday)
{
  if(MARCH == month && is_saturday_before_last_sunday_in_march_or_october(day, wday))
  {
    return 1;
  }

  if(OCTOBER == month && is_saturday_before_last_sunday_in_march_or_october(day, wday))
  {
    return -1;
  }

  return 0;
}

uint8_t get_last_hour_before_sleep_on_saturday()
{
  uint8_t l_hour = 23;

  while(0 == g_activity_table[5 * 24 + l_hour])
  {
    l_hour--;
  }

  return l_hour;
}

void set_dst_compensated_time_if_needed()
{
  const uint8_t l_day = g_rtc.getDay();
  const uint8_t l_wday = g_rtc.getWeekday();
  const uint8_t l_mon = g_rtc.getMonth();
  const uint8_t l_year = g_rtc.getYear();
  const uint8_t l_hour = g_rtc.getHour();
  const uint8_t l_min = g_rtc.getMinute();
  const uint8_t l_sec = g_rtc.getSecond();

  const int8_t l_dst_compensation = get_dst_compensation(l_mon, l_day, l_wday);

  if(0 != l_dst_compensation &&
     get_last_hour_before_sleep_on_saturday() == get_compensated_hour())
  {
    g_rtc.setDateTime(l_day,
                      l_wday,
                      l_mon,
                      is_battery_low_indication_set(),
                      l_year,
                      l_hour + l_dst_compensation,
                      l_min,
                      l_sec);
  }
}

bool is_battery_low_indication_set()
{
  return g_rtc.getCentury();
}

void set_battery_low_indication()
{
  g_rtc.setDateTime(g_rtc.getDay(),
                    g_rtc.getWeekday(),
                    g_rtc.getMonth(),
                    CENTURY_20TH,  /* no onboard RAM so needs hacking */
                    g_rtc.getYear(),
                    g_rtc.getHour(),
                    g_rtc.getMinute(),
                    g_rtc.getSecond());
}
